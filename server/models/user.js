var User
    , _ = require('underscore')
    , LocalStrategy = require('passport-local').Strategy
    , userRoles = require('../../client/src/routingConfig').userRoles
    , mongoose = require('mongoose');

var users = [
    {
        id: 1,
        username: "user",
        password: "password",
        role: userRoles.user
    },
    {
        id: 2,
        username: "manager",
        password: "password",
        role: userRoles.user
    },
    {
        id: 3,
        username: "admin",
        password: "password",
        role: userRoles.user
    },
    {
        id: 4,
        username: "developer",
        password: "password",
        role: userRoles.user
    },
    {
        id: 5,
        username: "tester",
        password: "password",
        role: userRoles.user
    }
];


var db = mongoose.connection;

db.on('error', console.error);

mongoose.connect('mongodb://localhost/test');

var logSchema = new mongoose.Schema({
    IP: String,
    Datetime: {type: Date, default: Date.now},
    Action: String,
    Username: String,
    Message: String
});


module.exports = {
    findAll: function () {
        return _.map(users, function (user) {
            return _.clone(user);
        });
    },

    findById: function (id) {
        return _.clone(_.find(users, function (user) {
            return user.id === id
        }));
    },

    findByUsername: function (username) {
        return _.clone(_.find(users, function (user) {
            return user.username === username;
        }));
    },
    localStrategy: new LocalStrategy(
        function (username, password, done) {
            var user = module.exports.findByUsername(username);

            if (!user) {
                done(null, false, {message: 'Incorrect username.'});
            }
            else if (user.password != password) {
                done(null, false, {message: 'Incorrect password.'});
            }
            else {
                return done(null, user);
            }
        }
    ),
    track: function (ip, action, username, message) {

        var Log = mongoose.model('Log', logSchema);

        var log = new Log({
            IP: ip,
            Action: action,
            Username: username,
            Message: message
        });

        log.save(function (err, log) {
            if (err) return console.error(err);
            console.dir(log);
        });

    },
    serializeUser: function (user, done) {
        done(null, user.id);
    },
    deserializeUser: function (id, done) {
        var user = module.exports.findById(id);

        if (user) {
            done(null, user);
        }
        else {
            done(null, false);
        }
    }
};