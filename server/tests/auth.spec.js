var expect = require('chai').expect
    , sinon = require('sinon')
    , AuthCtrl = require('../controllers/auth')
    , User = require('../../../models/User');

describe('Auth controller Unit Tests - ', function () {

    var req = {}
        , res = {}
        , next = {}
        , sandbox = sinon.sandbox.create();

    beforeEach(function () {

    });

    afterEach(function () {
        sandbox.restore();
    });

    describe('auth()', function () {

        beforeEach(function () {
            req.body = {
                username: "user",
                password: "pass",
                role: 1
            };
        });


        it('should call next() with an error argument if req.logIn() returns error', function (done) {

            req.logIn = function (user, callback) {
                return callback('SomeError');
            };

            next = function (err) {
                expect(err).to.exist;
                done();
            };

        });

    });
});