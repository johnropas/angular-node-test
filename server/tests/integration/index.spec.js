var app = require('../../app'),
    request = require('supertest'),
    passportStub = require('passport-stub');
passportStub.install(app);

// user account
var user = {
    'username':'newUser',
    'role':{bitMask: 2,title: "user"},
    'password':'12345'
};

// user account 2 - no role
var user2 = {
    'username':'newUser',
    'password':'12345'
};


describe('Server Integration Tests - ', function (done) {
    afterEach(function() {
        passportStub.logout(); // logout after each test
    });
    it('Homepage - Return a 200', function(done) {
        request(app).get('/').expect(200, done);
    });
    it('Logout - Return a 200', function(done) {
        request(app).post('/logout').expect(200, done);
    });
    it('Login as Admin - Return a 200', function(done) {
        request(app).post('/login').send(admin).expect(200, done);
    });

});