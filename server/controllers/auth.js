var passport = require('passport')
    , User = require('../models/User.js');

module.exports = {
    login: function (req, res, next) {

        var ip = req.connection.remoteAddress;
        var username = req.body.username;

        passport.authenticate('local', function (err, user, message) {
            if (err) {
                User.track(ip, 'AUTH_FAILURE', username, err.message);
                return next(err);
            }
            if (!user) {
                User.track(ip, 'AUTH_FAILURE', username, message);
                return res.send(message);
            }
            req.logIn(user, function (err) {
                if (err) {
                    User.track(ip, 'AUTH_FAILURE', username, err.message);
                    return next(err);
                }

                User.track(ip, 'AUTH_SUCCESS', username, '');
                if (req.body.rememberme) req.session.cookie.maxAge = 1000 * 60 * 60 * 24 * 7;
                res.json(200, {"role": user.role, "username": user.username});
            });
        })(req, res, next);
    },

    logout: function (req, res) {
        req.logout();
        res.send(200);
    }
};
//connection.remoteAddress