
module.exports = {
    item: function(req, res) {
        if(req.isAuthenticated())
            res.send("Private Resource Revealed");
        else
            res.send("Please login first");
    }
};