'use strict';

app.controller('HomeController', ['$rootScope', '$scope', '$location', 'Auth', function($rootScope, $scope, $location, Auth) {
    $scope.user = Auth.user;
    $scope.userRoles = Auth.userRoles;
    $scope.accessLevels = Auth.accessLevels;

    $scope.logout = function() {
        Auth.logout(function() {
            $location.path('/login');
        }, function() {
            $rootScope.error = "Failed to logout";
        });
    };
}]);

app.controller('LoginController',
    ['$rootScope', '$scope', '$location', '$window', 'Auth', function($rootScope, $scope, $location, $window, Auth) {

        $scope.rememberme = true;
        $scope.login = function() {
            Auth.login({
                    username: $scope.username,
                    password: $scope.password,
                    rememberme: $scope.rememberme
                },
                function(res) {
                    $location.path('/private');
                    if(res.message)$scope.message = 'Incorrect username or password';

                },
                function(err) {
                    $rootScope.error = "Failed to login";
                });
        };


    }]);

app.controller('PrivateController', function ($scope, $http) {

    $http.get('/resource').then(function (res){
        $scope.resource = res.data;
    })


});